<?php

namespace App\Controller;

use App\Service\ParserService;
use App\Utils\FilesystemHelper;
use App\Utils\Manager\FileUploadManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/parser", name="parser_")
 */
class ParserController extends AbstractController
{
    private $service;
    private $filesystemHelper;
    private $uploadManager;

    public function __construct(ParserService $service, FilesystemHelper $filesystemHelper, FileUploadManager $uploadManager)
    {
        $this->service = $service;
        $this->filesystemHelper = $filesystemHelper;
        $this->uploadManager = $uploadManager;
    }

    /**
     * @Route("/index", name="index")
     */
    public function index(): Response
    {
        try {
            $dbList = $this->filesystemHelper->getFileList($this->getParameter('db_upload_directory'));
            $exportList = $this->filesystemHelper->getFileList($this->getParameter('xml_upload_directory'));
        } catch (\Throwable $e) {
            return $this->json(['status' => 'fail']);
        }

        return $this->render('parser/index.html.twig', [
            'db_list' => $dbList,
            'export_list' => $exportList
        ]);
    }

    /**
     * @Route("/upload", methods={"POST"}, name="upload")
     */
    public function uploadAction(Request $request)
    {
        try {
            $dir = $this->filesystemHelper->getDirectoryPath($this->getParameter('db_upload_directory'));
            $this->uploadManager->upload($request, $dir);
        } catch (\Throwable $e) {
            return $this->json(['status' => 'fail']);
        }

        return $this->redirect('index');
    }

    /**
     * @Route("/export", methods={"POST"}, name="export")
     */
    public function exportAction(Request $request)
    {
        try {
        $this->service->export($request, $this->getParameter('db_upload_directory'), $this->getParameter('xml_upload_directory'));
        } catch (\Throwable $e) {
            return $this->json(['status' => 'fail']);
        }

        return $this->redirect('index');
    }

    /**
     * @Route("/merge", methods={"POST"}, name="merge")
     */
    public function mergeAction(Request $request)
    {
        try {
        $this->service->merge($request, $this->getParameter('xml_upload_directory'));
        } catch (\Throwable $e) {
            return $this->json(['status' => 'fail']);
        }

        return $this->redirect('index');
    }

    /**
     * @Route("/delete", methods={"POST"}, name="delete")
     */
    public function deleteAction(Request $request): Response
    {
        try {
            $this->service->delete($request, $this->getParameter('db_upload_directory'));
        } catch (\Throwable $e) {
            return $this->json(['status' => 'fail']);
        }

        return $this->redirect('index');
    }

    /**
     * @Route("/download", methods={"POST"}, name="download")
     */
    public function downloadAction(Request $request): Response
    {
        try {
            $this->service->download($request, $this->getParameter('xml_upload_directory'));
        } catch (\Throwable $e) {
            return $this->json(['status' => 'fail']);
        }

        return $this->redirect('index');
    }
}
