<?php


namespace App\Service;


use App\Utils\FileCreator;
use App\Utils\FilesystemHelper;
use App\Utils\SqlParser;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;

class ParserService
{
    private $fileSystem;
    private $filesystemHelper;
    private $parser;
    private $fileCreator;

    public function __construct(Filesystem $filesystem, FilesystemHelper $filesystemHelper, SqlParser $parser, FileCreator $fileCreator)
    {
        $this->fileSystem = $filesystem;
        $this->filesystemHelper = $filesystemHelper;
        $this->parser = $parser;
        $this->fileCreator = $fileCreator;
    }

    public function delete(Request $request, string $dir)
    {
        $selected = $request->request->get('select_db');
        if (!$selected) {
            return;
        }

        foreach ($selected as $file)
            $this->fileSystem->remove($this->filesystemHelper->getDirectoryPath($dir) . $file);
    }

    public function export(Request $request, string $parseDir, string $saveDir)
    {
        $selected = $request->request->get('select_db');
        if (!$selected) {
            return;
        }

        foreach ($selected as $file) {
            $data = $this->parser->parseTable($this->filesystemHelper->getDirectoryPath($parseDir) . $file);
            $this->fileCreator->create($data, $this->filesystemHelper->getDirectoryPath($saveDir));
        }
    }

    public function merge(Request $request, string $dir)
    {
        $selected = $request->request->get('select_export');
        if (count($selected) < 2) {
            return;
        }

        $files = [];
        foreach ($selected as $file) {
            $file = $this->filesystemHelper->getDirectoryPath($dir) . $file;
            $files[] = $file;
        }
        $this->fileCreator->merge($files, $this->filesystemHelper->getDirectoryPath($dir));
    }

    public function download(Request $request, string $dir)
    {
        $selected = $request->request->get('select_export');
        if (!$selected) {
            return;
        }
        $file = $this->filesystemHelper->getDirectoryPath($dir) . $selected[0];
        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . basename($file));
            header('Content-Length: ' . filesize($file));
            readfile($file);
            exit;
        }
    }
}