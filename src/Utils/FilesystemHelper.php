<?php


namespace App\Utils;


use Symfony\Component\Finder\Finder;

class FilesystemHelper
{
    private $projectDir;

    public function __construct($projectDir)
    {
        $this->projectDir = $projectDir;
    }

    public function getDirectoryPath($dir): string
    {
        return $this->projectDir . '/public' . $dir;
    }

    public function getFileList($dir): ?array
    {
        $path = $this->getDirectoryPath($dir);
        $finder = new Finder();
        $directory = $finder->files()->in($path);
        foreach ($directory as $file) {
            $list[] = $file->getFilename();
        }

        return $list ?? null;
    }
}