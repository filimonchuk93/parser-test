<?php


namespace App\Utils;

class SqlParser
{
    private $filesystemHelper;

    public function __construct(FilesystemHelper $filesystemHelper)
    {
        $this->filesystemHelper = $filesystemHelper;
    }

    public function parseDB(string $file): ?array
    {
        $file = fopen($file, 'r');
        $startPattern = '/INSERT INTO `[a-zA-Z0-9-_]*posts`/';
        $endPattern = '/;/';
        $needed = 0;
        $table = [];
        while (!feof($file)) {
            $line = fgets($file);
            $pos = preg_match($startPattern, $line);
            if ($pos || $needed == 1) {
                $needed = 1;
                $table[] = preg_replace('/\`/' ,'' ,$line);
                if (preg_match($endPattern, $line))
                    break;
            }
        }
        fclose($file);
        return $table;
    }

    public function getKeys(array $table, array $neededColumns = ['post_title', 'post_content'])
    {
        $tableFields = explode(', ', $table[0]);
        foreach ($neededColumns as $field) {
            $keys[$field] = array_search($field, $tableFields);
        }

        return $keys;
    }

    public function parseTable($file)
    {
        $table = $this->parseDB($file);
        $columnValues = [];
        $keys = $this->getKeys($table);
        $linkPattern = '/<a[^>]*>([^<]+)<\/a>/';
        $imgPattern = '/<img.*?src="(.*?)"[^\>]+>/';

        for ($i = 1; $i < count($table); $i++) {
            $line = str_replace(['\&gt;', '\&lt;' , '&laquo;' , '&raquo;'], ['>', '<', '«', '»'], $table[$i]);
            $line = preg_replace([$linkPattern, $imgPattern], '', $line);
            $line = explode(',' . chr(0x09), $line);
            foreach ($keys as $key => $value)
            $columnValues[$i][$key] = $line[$value];
        }
        return $columnValues;
    }
}