<?php


namespace App\Utils;


interface FileCreator
{
    /**
     * @param array $data
     * @param string $dir
     * @return void
     */
    public function create($data, $dir);

    /**
     * @param array $files
     * @param string $dir
     * @return void
     */
    public function merge($files, $dir);
}