<?php


namespace App\Utils\Generator;


use Symfony\Component\Finder\Finder;

class FileNameGenerator
{
    private $fileName;
    private $dir;

    public function __construct($fileName, $dir)
    {
        $this->fileName = $fileName;
        $this->dir = $dir;
    }

    public function isUniq()
    {
        $finder = new Finder();
        if ($finder->files()->in($this->dir)->name($this->fileName)->hasResults()) {
            return false;
        }
        return true;
    }

    public function generateUniqName()
    {
        if ($this->isUniq()) {
            return $this->fileName;
        }
        $fileNameArray = explode('.', $this->fileName);
        $this->fileName = $fileNameArray[0] . '_' . md5(uniqid('', true)) . '.' . $fileNameArray[1];

        return $this->generateUniqName();
    }
}