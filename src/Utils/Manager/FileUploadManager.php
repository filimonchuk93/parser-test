<?php


namespace App\Utils\Manager;


use App\Utils\Generator\FileNameGenerator;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;

class FileUploadManager
{
    public function upload(Request $request, string $dir)
    {
        $files = $request->files->get('files');

        $validator = Validation::createValidator();

        foreach ($files as $file) {
            if ($file instanceof UploadedFile) {
                $violations = $validator->validate($file, [
                    new Assert\File([
                        'mimeTypes' => [
                            'text/plain',
                            'application/sql'
                        ]
                    ]),
                ]);
                if (0 !== count($violations)) {
                    $fileErrors = [];
                    $fileErrors['file'] = $file->getClientOriginalName();
                    foreach ($violations as $violation) {
                        $fileErrors['errors'][] = $violation->getMessage();
                    }
                    $errors[] = $fileErrors;
                    continue;
                }
            }

            $fileNameGenerator = new FileNameGenerator($file->getClientOriginalName(), $dir);
            $fileName = $fileNameGenerator->generateUniqName();
            $file->move($dir, $fileName);
        }
    }

}