<?php


namespace App\Utils;


use App\Utils\Generator\FileNameGenerator;
use DOMDocument;

class XMLFileCreator implements FileCreator
{
    public function create($data, $dir)
    {
        $domtree = new DOMDocument('1.0', 'UTF-8');

        $posts = $domtree->createElement("posts");
        $posts = $domtree->appendChild($posts);
        foreach ($data as $value) {
            $post = $domtree->createElement('post');
            $post = $posts->appendChild($post);
            foreach ($value as $key => $item) {
                $post->appendChild($domtree->createElement($key, $item));
            }
        }
        $fileNameGenerator = new FileNameGenerator('export.xml', $dir);
        $domtree->save($dir . $fileNameGenerator->generateUniqName());
    }

    public function merge($files, $dir)
    {
        foreach ($files as $key => $value) {
            $file[$key] = new DOMDocument();
            $file[$key]->load($value);
        }

        $baseFile = $file[0]->getElementsByTagName('posts')->item(0);
        for ($i = 1; $i < count($file); $i++) {
            $posts = $file[$i]->getElementsByTagName('post');
            for ($j = 1; $j < $posts->length; $j++) {
                $item2 = $posts->item($j);
                $item1 = $file[0]->importNode($item2, true);
                $baseFile->appendChild($item1);
            }
        }

        $fileNameGenerator = new FileNameGenerator('marge_export.xml', $dir);
        $file[0]->save($dir . $fileNameGenerator->generateUniqName());
    }
}